const express = require("express");

const mongoose = require("mongoose");

const voucherRouter = require("./src/router/VoucherRouter");
const drinkRouter = require("./src/router/DrinkRouter");
const userRouter = require("./src/router/UserRouter");
const orderRouter = require("./src/router/OrderRouter");

const app = express();

const port = 8080;

//Su dung tieng Viet
app.use(express.urlencoded({
    extended: true
}));

//Su dung du lieu JSON
app.use(express.json());

async function connectMongoDB() {
    await mongoose.connect("mongodb://localhost:27017/Pizza365_Backend")
}

connectMongoDB()
.then(() => console.log(`Connect to MongoDB successful`))
.catch((error) => console.log(error))

app.get("/", (request, response) => {
    response.send(`CRUD Pizza365`)
})

app.use("/vouchers", voucherRouter);
app.use("/drinks", drinkRouter);
app.use("/users", userRouter);
app.use("/orders", orderRouter);

app.listen(port, () => {
    console.log(`App listening on ${port}`)
})
