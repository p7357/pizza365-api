const mongoose = require("mongoose");

const {Schema} = mongoose;

const date = new Date();

const orderSchema = new Schema({
    _id: Schema.Types.ObjectId,
    orderCode: {
        type: String,
        default: Math.random().toString(36).slice(-5)
    },
    pizzaSize: {
        type: String,
        require: true
    },
    pizzaType: {
        type: String,
        require: true
    },
    voucher: {
        type: String,
    },
    orderStatus: {
        type: String,
        default: `Created `
    },
    createdDate: {
        type: String,
        default: `${date.getHours()}:${date.getMinutes()} ${date.getDate()}/${date.getMonth()+1}/${date.getFullYear()}`
    }
})

const OrderModel = mongoose.model("orders", orderSchema);

module.exports = {OrderModel};