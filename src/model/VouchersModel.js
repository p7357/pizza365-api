const mongoose = require("mongoose");

const {Schema} = mongoose;

const voucherSchema = new Schema ({
    _id: Schema.Types.ObjectId,
    voucherId: {
        type: Number,
        required: true,
        unique: true,
        min: 10000,
        max: 99999,
    },
    voucherDiscount: {
        type: Number,
        required: true,
        min: 1,
        max: 100
    }
})

const VoucherModel = mongoose.model("vouchers", voucherSchema);

module.exports = {VoucherModel} ;