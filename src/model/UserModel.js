const mongoose = require("mongoose");

const {Schema} = mongoose;

const userSchema = new Schema({
    _id: Schema.Types.ObjectId,
    fullName: {
        type: String,
        require: true
    },
    email: {
        type: String,
        require: true
    },
    address: {
        type: String,
        require: true
    },
    phone: {
        type: String,
        require: true
    },
    orders: [
    {
        type: Schema.Types.ObjectId,
        ref: "orders"
    }
    ]
})

const UserModel = mongoose.model("users", userSchema);

module.exports = {UserModel};