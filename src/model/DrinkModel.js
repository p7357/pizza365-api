const mongoose = require("mongoose");

const {Schema} = mongoose;

const drinkSchema = new Schema ({
    _id: Schema.Types.ObjectId,
    drinkId: {
        type: String,
        required: true,
        unique: true,
    },
    drinkName: {
        type: String,
        required: true,
        unique: true,
    }
})

const DrinkModel = mongoose.model("drinks", drinkSchema)

module.exports = {DrinkModel}