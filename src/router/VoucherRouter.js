const express = require("express");

const router = express.Router();

const {createVoucher, getAllVoucher, getVoucher, updateVoucher, deleteVoucher} = require("../controller/VoucherController");

router.post("/", createVoucher);
router.get("/", getAllVoucher);

router.get("/:voucherId", getVoucher);
router.put("/:voucherId", updateVoucher);
router.delete("/:voucherId", deleteVoucher);

module.exports = router;