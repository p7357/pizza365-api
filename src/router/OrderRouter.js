const express = require("express");

const router = express.Router();

const {getAllOrders, updateOrder, getOrder, deleteOrder} = require("../controller/OrderController");

router.get("/", getAllOrders);

router.put("/:orderId", updateOrder);
router.delete("/:orderId", deleteOrder);
router.get("/:orderId", getOrder);


module.exports = router;