const express = require("express");

const router = express.Router();

const {createUser, getAllUsers, getUser, updateUser, deleteUser} = require("../controller/UserController");
const {createOrder, getAllUserOrder} = require("../controller/OrderController");

router.post("/", createUser);
router.get("/", getAllUsers);

router.get("/:userId", getUser);
router.put("/:userId", updateUser);
router.delete("/:userId", deleteUser);

router.post("/:userId/orders", createOrder);
router.get("/:userId/orders", getAllUserOrder);

module.exports = router;