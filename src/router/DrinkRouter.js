const express = require("express");

const router = express.Router();

const {createDrink, getAllDrink, getDrink, updateDrink, deleteDrink} = require("../controller/DrinkController");

router.post("/", createDrink);
router.get("/", getAllDrink);

router.get("/:drinkId", getDrink);
router.put("/:drinkId", updateDrink);
router.delete("/:drinkId", deleteDrink);

module.exports = router;