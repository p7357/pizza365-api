const mongoose = require("mongoose");

const {DrinkModel} = require("../model/DrinkModel");

function createDrink(request, response) {
    const voucher = new DrinkModel ({
        _id: mongoose.Types.ObjectId(),
        drinkId: request.body.drinkId,
        drinkName: request.body.drinkName
    })
    voucher.save()
    .then((newDrink) => {
        return response.status(200).json({
            message: "Success",
            drink: newDrink,
        }) 
    })
    .catch((error) => {
        return response.status(500).json({
            message:"Fail",
            error: error.message,
        })
    })
}

function getAllDrink(request, response) {
    DrinkModel.find()
    .select("_id drinkId drinkName")
    .then((drinkList) => {
        return response.status(200).json({
            message: "Success",
            drinkList: drinkList,
        }) 
    })
    .catch((error) => {
        return response.status(500).json({
            message:"Fail",
            error: error.message,
        })
    })
}

function getDrink(request, response) {
    const drinkId = request.params.drinkId;

    if(mongoose.Types.ObjectId.isValid(drinkId)){
        DrinkModel.findById(drinkId)
        .then((data) => {
            if(data){
                return response.status(200).json({
                    message: "Success",
                    drink: data
                })
            } else {
                return response.status(404).json({
                    message: "Fail",
                    error: "Not found"
                })
            }
        })
        .catch((error) => {
            return response.status(500).json({
                message:"Fail",
                error: error.message
            })
        })
    } else {
        return response.status(400).json({
            message: "Fail",
            error: "Drink Id is not valid"
        })
    }
}

function updateDrink(request, response) {
    const drinkId = request.params.drinkId;

    const body = request.body;

    if(mongoose.Types.ObjectId.isValid(drinkId)){
        DrinkModel.findByIdAndUpdate(drinkId, body)
        .then((data) => {
            if(data){
                return response.status(200).json({
                    message: "Success",
                    updateDrink: data
                })
            } else {
                return response.status(404).json({
                    message: "Fail",
                    error: "Not found"
                })
            }
        })
        .catch((error) => {
            return response.status(500).json({
                message: "Fail",
                error: error.message
            })
        })
    } else {
        return response.status(400).json({
            message: "Fail",
            error: "Drink Id is not valid"
        })
    }
}

function deleteDrink(request, response) {
    const drinkId = request.params.drinkId;

    if(mongoose.Types.ObjectId.isValid(drinkId)){
        DrinkModel.findByIdAndDelete(drinkId)
        .then((data) => {
            if(data){
                return response.status(200).json({
                    message: "Success",
                })
            } else {
                return response.status(404).json({
                    message: "Fail",
                    error: "Not found"
                })
            }
        })
        .catch((error) => {
            return response.status(500).json({
                message: "Fail",
                error: error.message
            })
        })
    } else {
        return response.status(400).json({
            message: "Fail",
            error: "Drink Id is not valid"
        })
    }
}

module.exports = {createDrink, getAllDrink, getDrink, updateDrink, deleteDrink}