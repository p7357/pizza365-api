const mongoose = require("mongoose");

const {VoucherModel} = require("../model/VouchersModel")

function createVoucher(request, response) {
    const voucher = new VoucherModel ({
        _id: mongoose.Types.ObjectId(),
        voucherId: request.body.voucherId,
        voucherDiscount: request.body.voucherDiscount
    })
    voucher.save()
    .then((newVoucher) => {
        return response.status(200).json({
            message: "Success",
            voucher: newVoucher,
        }) 
    })
    .catch((error) => {
        return response.status(500).json({
            message:"Fail",
            error: error.message,
        })
    })
}

function getAllVoucher(request, response){
    VoucherModel.find()
    .select("_id voucherId voucherDiscount")
    .then((voucherList) => {
        return response.status(200).json({
            message: "Success",
            voucherList: voucherList,
        }) 
    })
    .catch((error) => {
        return response.status(500).json({
            message:"Fail",
            error: error.message,
        })
    })
}

function getVoucher(request, response) {
    const voucherId = request.params.voucherId;

    if(mongoose.Types.ObjectId.isValid(voucherId)){
        VoucherModel.findById(voucherId)
        .then((data) => {
            if(data){
                return response.status(200).json({
                    message: "Success",
                    voucher: data
                })
            } else {
                return response.status(404).json({
                    message: "Fail",
                    error: "Not found"
                })
            }
        })
        .catch((error) => {
            return response.status(500).json({
                message: "Fail",
                error: error.message
            })
        })
    } else {
        return response.status(400).json({
            message: "Fail",
            error: "Voucher Id is not valid"
        })
    }
}

function updateVoucher(request, response) {
    const voucherId = request.params.voucherId;
    const body = request.body;

    if(mongoose.Types.ObjectId.isValid(voucherId)){
        VoucherModel.findByIdAndUpdate(voucherId, body)
        .then((data) => {
            if(data){
                return response.status(200).json({
                    message: "Success",
                    voucherUpdate: data
                })
            } else {
                return response.status(404).json({
                    message: "Fail",
                    error: "Not found"
                })
            }
        })
        .catch((error) => {
            return response.status(500).json({
                message: "Fail",
                error: error.message
            })
        })
    } else {
        return response.status(400).json({
            message: "Fail",
            error: "Voucher Id is not valid"
        })
    }
}

function deleteVoucher(request, response) {
    const voucherId = request.params.voucherId;

    if(mongoose.Types.ObjectId.isValid(voucherId)){
        VoucherModel.findByIdAndDelete(voucherId)
        .then((data) => {
            if(data){
                return response.status(200).json({
                    message: "Success",
                })
            } else {
                return response.status(404).json({
                    message: "Fail",
                    error: "Not found"
                })
            }
        })
        .catch((error) => {
            return response.status(500).json({
                message: "Fail",
                error: error.message
            })
        })
    } else {
        return response.status(400).json({
            message: "Fail",
            error: "Voucher Id is not valid"
        })
    }

}

module.exports = {createVoucher, getAllVoucher, getVoucher, updateVoucher, deleteVoucher}