const mongoose = require("mongoose");

const {OrderModel} = require("../model/OrderModel");
const {UserModel} = require("../model/UserModel");

function createOrder(req, res) {
    const order = new OrderModel({
        _id: mongoose.Types.ObjectId(),
        pizzaSize: req.body.pizzaSize,
        pizzaType: req.body.pizzaType,
        voucher: req.body.voucher 
    });

    const userId = req.params.userId;
    if(mongoose.Types.ObjectId.isValid(userId)){
        order.save()
        .then((newOrder) => {
            return UserModel.findOneAndUpdate({_id: userId}, {$push: {orders: newOrder._id}}, {new: true})
        })
        .then((updateOrder) => {
            if(updateOrder){
                return res.status(200).json({
                    success: true,
                    updateOrder: updateOrder
                })
            } else {
                return res.status(404).json({
                    success: false,
                    error: "User Id is not found"
                })
            }
        })
        .catch((error) => {
            return res.status(500).json({
                success: false,
                error: error.message
            })
        })
    } else {
        return res.status(400).json({
            success: false,
            error: "User Id is not valid"
        })
    }
}

function getAllUserOrder(req, res) {
    const userId = req.params.userId;
    if(mongoose.Types.ObjectId.isValid(userId)) {
        UserModel.findById(userId)
        .populate("orders")
        .then((orderList) => {
            if(orderList){
                return res.status(200).json({
                    success: true,
                    orderList: orderList
                })
            } else {
                return res.status(404).json({
                    success: false,
                    error: "User Id is not found"
                })
            }
        })
        .catch((error) => {
            return res.status(500).json({
                success: false,
                error: error.message
            })
        })
    } else {
        return res.status(400).json({
            success: false,
            error: "User Id is not valid"
        })
    }
}

function getAllOrders(req, res) {
    OrderModel.find()
    .select("_id orderCode pizzaSize pizzaType voucher orderStatus createdDate")
    .then((orderList) => {
        return res.status(200).json({
            success: true,
            orderList: orderList
        })
    })
    .catch((error) => {
        return res.status(500).json({
            success: false,
            error: error.message
        })
    })
}

function getOrder(req, res) {
    const orderId = req.params.orderId;

    if(mongoose.Types.ObjectId.isValid(orderId)){
        OrderModel.findById(orderId)
        .then((data) => {
            if(data){
                return res.status(200).json({
                    success: true,
                    order: data
                })
            } else {
                return res.status(404).json({
                    success: false,
                    error: "Order Id is not found"
                })
            }
        })
        .catch((error) => {
            return res.status(500).json({
                success: false,
                error: error.message
            })
        })
    } else {
        return res.status(400).json({
            success: false,
            error: "Order Id is not valid"
        })
    }

}

function updateOrder(req, res) {
    const orderId = req.params.orderId;
    const body = req.body;

    if(mongoose.Types.ObjectId.isValid(orderId)){
        OrderModel.findByIdAndUpdate(orderId, body)
        .then((updateOrder) => {
            if(updateOrder){
                return res.status(200).json({
                    success: true,
                    updateOrder: updateOrder
                })
            } else {
                return res.status(404).json({
                    success: false,
                    error: "Order Id is not found"
                })
            }
        })
        .catch((error) => {
            return res.status(500).json({
                success: false,
                error: error.message
            })
        })
    } else {
        return res.status(400).json({
            success: false,
            error: "Order Id is not valid"
        })
    }
}

function deleteOrder(req, res) {
    const orderId = req.params.orderId;

    if(mongoose.Types.ObjectId.isValid(orderId)){
        OrderModel.findByIdAndDelete(orderId)
        .then((data) => {
            if(data){
                return res.status(200).json({
                    success: true,
                })
            } else {
                return res.status(404).json({
                    success: false,
                    error: "Order Id is not found"
                })
            }
        })
        .catch((error) => {
            return res.status(500).json({
                success: false,
                error: error.message
            })
        })
    } else {
        return res.status(400).json({
            success: false,
            error: "Order Id is not valid"
        })
    }
}

module.exports = {createOrder, getAllUserOrder, getAllOrders, updateOrder, getOrder, deleteOrder}