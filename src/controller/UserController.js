const mongoose = require("mongoose");

const {UserModel} = require("../model/UserModel");

function createUser(req, res) {
    const user = new UserModel({
        _id: mongoose.Types.ObjectId(),
        fullName: req.body.fullName,
        email: req.body.email,
        address: req.body.address,
        phone: req.body.phone,
    })

    user.save()
    .then((data) => {
        return res.status(200).json({
            success: true,
            newUser: data
        })
    })
    .then((error) => {
        return res.status(500).json({
            success: false,
            error: error.message
        })
    })
}

function getAllUsers(req, res) {
    UserModel.find()
    .select("_id fullName email address phone orders")
    .then((userList) => {
        return res.status(200).json({
            success: true,
            userList: userList
        })
    })
    .then((error) => {
        return res.status(500).json({
            success: false,
            error: error.message
        })
    })
}

function getUser(req, res) {
    const userId = req.params.userId;

    if(mongoose.Types.ObjectId.isValid(userId)){
        UserModel.findById(userId)
        .then((data) => {
            if(data){
                return res.status(200).json({
                    success: true,
                    user: data
                })
            } else {
                return res.status(404).json({
                    success: false,
                    error: "User Id is not found"
                })
            }
        })
        .catch((error) => {
            return res.status(500).json({
                success: false,
                error: error.message
            })
        })
    } else {
        return res.status(400).json({
            success: false,
            error: "User Id is not valid"
        })
    }
}

function updateUser(req, res) {
    const userId = req.params.userId;
    const body = req.body;

    if(mongoose.Types.ObjectId.isValid(userId)){
        UserModel.findByIdAndUpdate(userId, body)
        .then((data) => {
            if(data){
                return res.status(200).json({
                    success: true,
                    userUpdate: data
                })
            } else {
                return res.status(404).json({
                    success: false,
                    error: "User Id is not found"
                })
            }
        })
        .catch((error) => {
            return res.status(500).json({
                success: false,
                error: error.message
            })
        })
    } else {
        return res.status(400).json({
            success: false,
            error: "User Id is not valid"
        })
    }
}

function deleteUser(req, res) {
    const userId = req.params.userId;

    if(mongoose.Types.ObjectId.isValid(userId)){
        UserModel.findByIdAndDelete(userId)
        .then((data) => {
            if(data){
                return res.status(200).json({
                    success: true,
                })
            } else {
                return res.status(404).json({
                    success: false,
                    error: "User Id is not found"
                })
            }
        })
        .catch((error) => {
            return res.status(500).json({
                success: false,
                error: error.message
            })
        })
    } else {
        return res.status(400).json({
            success: false,
            error: "User Id is not valid"
        })
    }
}


module.exports = {createUser, getAllUsers, getUser, updateUser, deleteUser}